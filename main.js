function addNota() {
    let nota = input.value
    input.value = ""

    if (isNaN(nota) || nota == "") {
        alert("Por favor, insira uma nota válida!");
        return;
    }

    floatNota = parseFloat(nota);
    if (0 > floatNota || floatNota > 10) {
        alert("Por favor, insira uma nota entre 0 e 10!");
        return;
    }

    // adiciona nota no array de notas
    notas.push(floatNota);

    // adiciona a nota no log de notas
    log.textContent += "A nota " + notas.length + " foi " + nota + "\n";
}

function calcMedia() {
    let total = 0;
    for (let i = 0; i < notas.length; ++i) {
        total += notas[i];
    }
    
    let media = total / notas.length;

    output.textContent = media.toString();
}


var notas = [];
input = document.getElementById("input-nota");
output = document.getElementById("output")
log = document.getElementById("log");

addNotaButton = document.getElementById("button-add");
addNotaButton.addEventListener("click", addNota);

calcMediaButton = document.getElementById("button-media");
calcMediaButton.addEventListener("click", calcMedia);
